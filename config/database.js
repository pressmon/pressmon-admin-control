module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', '192.168.7.1'),
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'db_indonesia'),
        username: env('DATABASE_USERNAME', 'global'),
        password: env('DATABASE_PASSWORD', 'global'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
