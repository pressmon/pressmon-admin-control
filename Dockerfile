FROM node:lts

WORKDIR cakepin/chaca_tk/

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 2337


cmd ["npm","run","build"]
CMD ["npm","start"]
